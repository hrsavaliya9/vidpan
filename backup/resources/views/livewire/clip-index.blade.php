@section('meta')
    <title>{{ __('Clips') }} - {{ __('Backend') }} | {{ config('app.name') }}</title>
@endsection

<div>
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ config('app.name') }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ __('Clips') }}</li>
            </ol>
        </nav>
        @include('partials.flash')
        <div class="card shadow-sm">
            <div class="card-body">
                <div class="spinner-border spinner-border-sm float-right" role="status" wire:loading>
                    <span class="sr-only">{{ __('Loading') }}&hellip;</span>
                </div>
                <h5 class="card-title text-primary">{{ __('Clips') }}</h5>
                <p class="card-text">{{ __('List and manage uploaded clips here.') }}</p>
            </div>
            <div class="card-body border-top">
                <div class="row">
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="form-group mb-lg-0">
                            <label for="filter-search">{{ __('Search') }}</label>
                            <input id="filter-search" class="form-control" placeholder="{{ __('Enter user name or email') }}&hellip;" wire:model.debounce.500ms="search">
                        </div>
                    </div>
                    @php
                        $sections = App\ClipSection::query()->orderBy('order')->get();
                    @endphp
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="form-group mb-lg-0">
                            <label for="filter-section">{{ __('Section') }}</label>
                            <select id="filter-section" class="form-control" wire:model="section">
                                <option value="">{{ __('Any') }}</option>
                                @foreach ($sections as $section)
                                    <option value="{{ $section->id }}">{{ $section->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="form-group mb-sm-0">
                            <label for="filter-language">{{ __('Language') }}</label>
                            <select id="filter-language" class="form-control" wire:model="language">
                                <option value="">{{ __('Any') }}</option>
                                @foreach (config('fixtures.languages') as $code => $name)
                                    <option value="{{ $code }}">{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="form-group mb-0">
                            <label for="filter-length">{{ __('Length') }}</label>
                            <select id="filter-length" class="form-control" wire:model="length">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th></th>
                        <th>{{ __('Description') }}</th>
                        <th>{{ __('Language') }}</th>
                        <th>{{ __('Approved?') }}</th>
                        <th>{{ __('Created at') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($clips as $clip)
                        <tr>
                            <td>{{ $clip->id }}</td>
                            <td>
                                <div class="text-hide img-screenshot rounded" style="background-image: url('{{ Storage::cloud()->url($clip->screenshot) }}')">&nbsp;</div>
                            </td>
                            <td>
                                @if ($clip->description)
                                    {{ $clip->description_short }}
                                @else
                                    <span class="text-muted">{{ __('Empty') }}</span>
                                @endif
                            </td>
                            <td>{{ config('fixtures.languages.' . $clip->language) }}</td>
                            <td>
                                @if ($clip->approved)
                                    <i class="fas fa-toggle-on mr-1 text-success"></i>
                                @else
                                    <i class="fas fa-toggle-off mr-1 text-danger"></i>
                                @endif
                            </td>
                            <td>{{ $clip->created_at->format('d/m/Y H:i:s') }}</td>
                            <td>
                                <a class="btn btn-outline-dark btn-sm" href="{{ route('clips.show', $clip) }}">
                                    <i class="fas fa-eye mr-1"></i> {{ __('Details') }}
                                </a>
                                <a class="btn btn-info btn-sm" href="{{ route('clips.update', $clip) }}">
                                    <i class="fas fa-feather mr-1"></i> {{ __('Edit') }}
                                </a>
                                @can('administer')
                                    <a class="btn btn-danger btn-sm" href="{{ route('clips.destroy', $clip) }}">
                                        <i class="fas fa-trash mr-1"></i> {{ __('Delete') }}
                                    </a>
                                @endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-center text-muted" colspan="7">{{ __('Could not find any clips to show.') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            @if ($clips->hasPages())
                <div class="card-body border-top">
                    {{ $clips->onEachSide(1)->links() }}
                </div>
            @endif
            <div class="card-body border-top">
                {{ __('Showing :from to :to of :total clips.', ['from' => $clips->firstItem() ?: 0, 'to' => $clips->lastItem() ?: 0, 'total' => $clips->total()]) }}
            </div>
        </div>
    </div>
</div>
