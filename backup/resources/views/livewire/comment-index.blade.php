@section('meta')
    <title>{{ __('Comments') }} - {{ __('Backend') }} | {{ config('app.name') }}</title>
@endsection

<div>
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ config('app.name') }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ __('Comments') }}</li>
            </ol>
        </nav>
        @include('partials.flash')
        <div class="card shadow-sm">
            <div class="card-body">
                <div class="spinner-border spinner-border-sm float-right" role="status" wire:loading>
                    <span class="sr-only">{{ __('Loading') }}&hellip;</span>
                </div>
                <h5 class="card-title text-primary">{{ __('Comments') }}</h5>
                <p class="card-text">{{ __('List and manage uploaded comments here.') }}</p>
            </div>
            <div class="card-body border-top">
                <div class="row">
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="form-group mb-sm-0">
                            <label for="filter-search">{{ __('Search') }}</label>
                            <input id="filter-search" class="form-control" placeholder="{{ __('Enter user name or email') }}&hellip;" wire:model.debounce.500ms="search">
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3 offset-md-4 offset-lg-6">
                        <div class="form-group mb-0">
                            <label for="filter-length">{{ __('Length') }}</label>
                            <select id="filter-length" class="form-control" wire:model="length">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table mb-0">
                    <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th></th>
                        <th>{{ __('User') }}</th>
                        <th>{{ __('Comment') }}</th>
                        <th>{{ __('Created at') }}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($comments as $comment)
                        <tr>
                            <td>{{ $comment->id }}</td>
                            <td>
                                <div class="text-hide img-screenshot rounded" style="background-image: url('{{ Storage::cloud()->url($comment->commentable->screenshot) }}')">&nbsp;</div>
                            </td>
                            <td>{{ $comment->commentator->name }}</td>
                            <td>{{ $comment->comment_short }}</td>
                            <td>{{ $comment->created_at->format('d/m/Y H:i:s') }}</td>
                            <td>
                                <a class="btn btn-outline-dark btn-sm" href="{{ route('comments.show', $comment) }}">
                                    <i class="fas fa-eye mr-1"></i> {{ __('Details') }}
                                </a>
                                @can('administer')
                                    <a class="btn btn-danger btn-sm" href="{{ route('comments.destroy', $comment) }}">
                                        <i class="fas fa-trash mr-1"></i> {{ __('Delete') }}
                                    </a>
                                @endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-center text-muted" colspan="6">{{ __('Could not find any comments to show.') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            @if ($comments->hasPages())
                <div class="card-body border-top">
                    {{ $comments->onEachSide(1)->links() }}
                </div>
            @endif
            <div class="card-body border-top">
                {{ __('Showing :from to :to of :total comments.', ['from' => $comments->firstItem() ?: 0, 'to' => $comments->lastItem() ?: 0, 'total' => $comments->total()]) }}
            </div>
        </div>
    </div>
</div>
