@if ($activities->isNotEmpty())
    <ul class="activity-logs">
        @foreach ($activities as $activity)
            <li>
                <div class="card shadow-sm mb-3">
                    <div class="card-body">
                        <p class="card-text">{{ $activity->description }}</p>
                    </div>
                    @php
                        /** @var \Illuminate\Support\Collection $properties */
                        $properties = $activity->changes();
                    @endphp
                    <div class="table-responsive">
                        <table class="table table-sm mb-0">
                            <tbody>
                            @foreach($properties['attributes'] ?? [] as $key => $value)
                                <tr>
                                    <th>{{ ucfirst(implode(' ', explode('_', $key))) }}:</th>
                                    <td class="w-100">
                                        @if (in_array($key, ['email', 'phone']) && config('fixtures.demo_mode'))
                                            <span class="text-muted">{{ __('Redacted in demo') }}</span>
                                        @elseif (is_array($value))
                                            {{ __(':count Item(s)', ['count' => count($value)]) }}
                                        @elseif (is_bool($value))
                                            {{ $value ? 'Yes' : 'No' }}
                                        @elseif (is_string($value))
                                            @if (mb_strlen($value) > 20)
                                                {{ mb_substr($value, 0, 20) }}&hellip;
                                            @elseif (empty($value))
                                                <span class="text-muted">{{ __('Empty') }}</span>
                                            @else
                                                {{ $value }}
                                            @endif
                                        @elseif (is_null($value))
                                            <span class="text-muted">{{ __('Empty') }}</span>
                                        @else
                                            {{ $value }}
                                        @endif
                                        @if ($properties->has('old') && array_key_exists($key, $properties['old']))
                                            <del>
                                                @php
                                                    $value = $properties['old'][$key];
                                                @endphp
                                                @if (in_array($key, ['email', 'phone']) && config('fixtures.demo_mode'))
                                                    <span class="text-muted">{{ __('Redacted in demo') }}</span>
                                                @elseif (is_array($value))
                                                    {{ __(':count Item(s)', ['count' => count($value)]) }}
                                                @elseif (is_bool($value))
                                                    {{ $value ? 'Yes' : 'No' }}
                                                @elseif (is_string($value))
                                                    @if (mb_strlen($value) > 20)
                                                        {{ mb_substr($value, 0, 20) }}&hellip;
                                                    @elseif (empty($value))
                                                        <span class="text-muted">{{ __('Empty') }}</span>
                                                    @else
                                                        {{ $value }}
                                                    @endif
                                                @elseif (is_null($value))
                                                    <span class="text-muted">{{ __('Empty') }}</span>
                                                @else
                                                    {{ $value }}
                                                @endif
                                            </del>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer border-top">
                        <p class="text-right mb-0">
                            <small>
                                <span class="text-muted">{{ __('at') }}</span>
                                {{ $activity->created_at->format('d/m/Y H:i') }}
                                <span class="text-muted">{{ __('by') }}</span>
                                @if ($activity->causer)
                                    <a href="{{ route('users.show', $activity->causer) }}">{{ $activity->causer->name }}</a>
                                @else
                                    <span class="text-muted">{{ __('Unknown') }}</span>
                                @endif
                            </small>
                        </p>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
@else
    <div class="bg-light p-2">
        <p class="text-muted text-center mb-0">{{ __('No activity logged.') }}</p>
    </div>
@endif
