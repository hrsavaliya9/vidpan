<?php

namespace App\Http\Livewire;

use App\Comment;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class CommentIndex extends Component
{
    use WithPagination;

    public $search;

    public $length;

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = Comment::query();
        if ($this->search) {
            $query->where('comment', 'line', "%$this->search%")
                ->orWhere(function (Builder $query) {
                    $query->where('comment', 'like', "%$this->search%")
                        ->orWhereHas('commentator', function (Builder $query) {
                            $query->where('name', 'like', "%$this->search%")
                                ->orWhere('email', 'like', "%$this->search%")
                                ->orWhere('username', 'like', "%$this->search%");
                        });
                });
        }

        $comments = $query->latest()->paginate($this->length);
        return view('livewire.comment-index', compact('comments'));
    }
}
