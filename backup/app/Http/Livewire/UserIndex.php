<?php

namespace App\Http\Livewire;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\QueryException;
use Livewire\Component;
use Livewire\WithPagination;

class UserIndex extends Component
{
    use WithPagination;

    public $search;

    public $length;

    public $role;

    public $verified;

    public $enabled = 'true';

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingRole()
    {
        $this->resetPage();
    }

    public function updatingVerified()
    {
        $this->resetPage();
    }

    public function updatingEnabled()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = User::query();
        if ($this->search) {
            $query->where(function (Builder $query) {
                $query->where('name', 'like', "%$this->search%")
                    ->orWhere('email', 'like', "%$this->search%")
                    ->orWhere('phone', 'like', "%$this->search%")
                    ->orWhere('username', 'like', "%$this->search%");
            });
        }

        if ($this->role) {
            $query->where('role', $this->role);
        }

        if ($this->verified) {
            $query->where('verified', $this->verified === 'true');
        }

        if ($this->enabled) {
            $query->where('enabled', $this->enabled === 'true');
        }

        $users = $query->latest()->paginate($this->length);
        return view('livewire.user-index', compact('users'));
    }

    public function suggest($id)
    {
        /** @var User $user */
        $user = User::query()->findOrFail($id);
        try {
            $user->suggestion()->create(['order' => 99]);
        } catch (QueryException $e) {
        }

        session()->flash('success', __(':name has been added to suggested users.', ['name' => $user->name]));
        return $this->render();
    }
}
