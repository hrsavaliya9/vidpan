<?php

namespace App\Http\Livewire;

use App\Verification;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class VerificationIndex extends Component
{
    use WithPagination;

    public $search;

    public $length;

    public $status;

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingStatus()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = Verification::query();
        if ($this->search) {
            $query->where('status', 'like', "%$this->search%")
                ->orWhereHas('user', function (Builder $query) {
                    $query->where('name', 'like', "%$this->search%")
                        ->orWhere('email', 'like', "%$this->search%")
                        ->orWhere('phone', 'like', "%$this->search%")
                        ->orWhere('username', 'like', "%$this->search%");
                });
        }

        if ($this->status) {
            $query->where('status', $this->status);
        }

        $verifications = $query->latest()->with('user')->paginate($this->length);
        return view('livewire.verification-index', compact('verifications'));
    }
}
