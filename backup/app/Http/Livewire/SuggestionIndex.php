<?php

namespace App\Http\Livewire;

use App\Suggestion;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class SuggestionIndex extends Component
{
    use WithPagination;

    public $search;

    public $length;

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = Suggestion::query();
        if ($this->search) {
            $query->whereHas('user', function (Builder $query) {
                $query->where('name', 'like', "%$this->search%")
                    ->orWhere('email', 'like', "%$this->search%")
                    ->orWhere('username', 'like', "%$this->search%");
            });
        }

        $suggestions = $query->orderBy('order')->paginate($this->length);
        return view('livewire.suggestion-index', compact('suggestions'));
    }
}
