<?php

namespace App\Http\Livewire;

use App\Report;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class ReportIndex extends Component
{
    use WithPagination;

    public $search;

    public $length;

    public $subject;

    public $status = 'received';

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingSubject()
    {
        $this->resetPage();
    }

    public function updatingStatus()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = Report::query();
        if ($this->search) {
            $query->where(function (Builder $query) {
                $query->where('reason', 'like', "%$this->search%")
                    ->orWhere('message', 'like', "%$this->search%");
            });
        }

        if ($this->subject) {
            $query->where('subject_type', $this->subject);
        }

        if ($this->status) {
            $query->where('status', $this->status);
        }

        $reports = $query->latest()->paginate($this->length);
        return view('livewire.report-index', compact('reports'));
    }
}
