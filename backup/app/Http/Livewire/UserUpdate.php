<?php

namespace App\Http\Livewire;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Livewire\Component;

class UserUpdate extends Component
{
    /** @var User */
    public $user;

    public $name;

    public $username;

    public $email;

    public $phone;

    public $password;

    public $role;

    public $enabled;

    public $verified;

    public function mount(User $user)
    {
        if ($user->can('manage') && !Gate::check('administer')) {
            abort(403);
        }

        $this->user = $user;
        $this->fill($user);
    }

    public function render()
    {
        return view('livewire.user-update');
    }

    public function update()
    {
        $data = $this->validate([
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'regex:/^\w[\w.]+\w$/', 'min:3', 'max:30'],
            'email' => ['nullable', 'string', 'email', 'max:255'],
            'phone' => ['nullable', 'string', 'regex:/^\+\d+$/', 'max:15'],
            'password' => ['nullable', 'string', 'min:8'],
            'role' => ['nullable', 'string', Rule::in(array_keys(config('fixtures.user_roles')))],
            'enabled' => ['nullable', 'boolean'],
            'verified' => ['nullable', 'boolean'],
        ]);
        $exists = User::query()
            ->whereKeyNot($this->user->id)
            ->where('username', $data['username'])
            ->exists();
        if ($exists) {
            throw ValidationException::withMessages([
                'username' => __('validation.unique', ['attribute' => 'username'])
            ]);
        }

        if (empty($data['password'])) {
            unset($data['password']);
        } else {
            $data['password'] = Hash::make($data['password']);
        }

        if (!Gate::check('administer')) {
            unset($data['role']);
        }

        $this->user->fill($data);
        $this->user->verified = !empty($data['verified']);
        $this->user->save();
        session()->flash('info', __('User :name has been updated.', ['name' => $this->user->name]));
        return redirect()->route('users.show', $this->user);
    }
}
