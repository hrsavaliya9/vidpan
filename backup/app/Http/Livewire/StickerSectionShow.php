<?php

namespace App\Http\Livewire;

use App\Sticker;
use App\StickerSection;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;

class StickerSectionShow extends Component
{
    use WithFileUploads;

    public $section;

    public $image;

    public function mount(StickerSection $section)
    {
        $this->section = $section;
    }

    public function render()
    {
        $activities = $this->section->activities()->latest()->paginate();
        return view('livewire.sticker-section-show', compact('activities'));
    }

    public function upload()
    {
        $data = $this->validate([
            'image' => [
                'required',
                'image',
                'mimes:jpeg,jpg,png',
                'max:' . config('fixtures.upload_limits.sticker.image'),
                'dimensions:min_width=256,min_height=256,max_width:1024,max_height:1024',
            ],
        ]);
        /** @var UploadedFile $image */
        $image = $data['image'];
        $name = Str::random(15) . '.' . $image->guessExtension();
        $data['image'] = $image->storePubliclyAs('stickers', $name, config('filesystems.cloud'));
        $sticker = $this->section->stickers()->create($data);
        session()->flash('success', __('Sticker :image has been successfully added.', ['image' => basename($sticker->image)]));
        return redirect()->route('sticker-sections.show', $this->section);
    }
}
