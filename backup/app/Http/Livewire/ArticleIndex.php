<?php

namespace App\Http\Livewire;

use App\Article;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class ArticleIndex extends Component
{
    use WithPagination;

    public $search;

    public $length;

    public $section;

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingSection()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = Article::query();
        if ($this->search) {
            $query->where(function (Builder $query) {
                $query->where('title', 'like', "%$this->search%")
                    ->orWhere('source', 'like', "%$this->search%");
            });
        }

        if ($this->section) {
            $query->whereHas('sections', function (Builder $query) {
                $query->whereKey($this->section);
            });
        }

        $articles = $query->latest()->paginate($this->length);
        return view('livewire.article-index', compact('articles'));
    }
}
