<?php

namespace App\Http\Livewire;

use App\NotificationTemplate;
use Livewire\Component;
use Livewire\WithPagination;

class NotificationTemplateIndex extends Component
{
    use WithPagination;

    public $search;

    public $length;

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = NotificationTemplate::query();
        if ($this->search) {
            $query->where('title', 'like', "%$this->search%")
                ->orWhere('body', 'like', "%$this->search%");
        }

        $templates = $query->latest()->paginate($this->length);
        return view('livewire.notification-template-index', compact('templates'));
    }
}
