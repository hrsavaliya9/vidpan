<?php

namespace App\Http\Livewire;

use App\NotificationSchedule;
use Livewire\Component;
use Livewire\WithPagination;

class NotificationScheduleIndex extends Component
{
    use WithPagination;

    public $search;

    public $length;

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = NotificationSchedule::query();
        if ($this->search) {
            $query->where('time', 'like', "%$this->search%");
        }

        $schedules = $query->latest()->paginate($this->length);
        return view('livewire.notification-schedule-index', compact('schedules'));
    }
}
