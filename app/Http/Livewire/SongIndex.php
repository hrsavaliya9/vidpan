<?php

namespace App\Http\Livewire;

use App\Song;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class SongIndex extends Component
{
    use WithPagination;

    public $search;

    public $section;

    public $length;

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingSection()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = Song::query();
        if ($this->search) {
            $query->where(function (Builder $query) {
                $query->where('title', 'like', "%$this->search%")
                    ->orWhere('artist', 'like', "%$this->search%")
                    ->orWhere('album', 'like', "%$this->search%");
            });
        }

        if ($this->section) {
            $query->whereHas('sections', function (Builder $query) {
                $query->whereKey($this->section);
            });
        }

        $songs = $query->latest()->paginate($this->length);
        return view('livewire.song-index', compact('songs'));
    }
}
