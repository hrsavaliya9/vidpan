<?php

namespace App\Http\Livewire;

use App\SongSection;
use Livewire\Component;
use Livewire\WithPagination;

class SongSectionIndex extends Component
{
    use WithPagination;

    public $search;

    public $length;

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = SongSection::query();
        if ($this->search) {
            $query->where('name', 'like', "%$this->search%");
        }

        $sections = $query->latest()->paginate($this->length);
        return view('livewire.song-section-index', compact('sections'));
    }
}
