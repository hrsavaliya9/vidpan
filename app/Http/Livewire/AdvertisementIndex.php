<?php

namespace App\Http\Livewire;

use App\Advertisement;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class AdvertisementIndex extends Component
{
    use WithPagination;

    public $search;

    public $length;

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = Advertisement::query();
        if ($this->search) {
            $query->where(function (Builder $query) {
                $query->where('type', 'like', "%$this->search%")
                    ->orWhere('location', 'like', "%$this->search%")
                    ->orWhere('network', 'like', "%$this->search%")
                    ->orWhere('link', 'like', "%$this->search%");
            });
        }

        $advertisements = $query->latest()->paginate($this->length);
        return view('livewire.advertisement-index', compact('advertisements'));
    }
}
