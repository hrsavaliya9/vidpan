<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Artisan;
use Livewire\Component;

class ConfigUpdate extends Component
{
    public $env;

    public function mount()
    {
        $this->env = file_get_contents(base_path('.env'));
    }

    public function render()
    {
        return view('livewire.config-update');
    }

    public function update()
    {
        $data = $this->validate([
            'env' => ['required', 'string'],
        ]);
        file_put_contents(base_path('.env'), $data['env']);
        session()->flash('info', __('Environment file has been successfully rewritten.'));
        return redirect()->route('config.update');
    }

    public function refresh()
    {
        Artisan::call('config:cache', []);
        session()->flash('info', __('Configuration has been refresh with latest .env contents.'));
        return redirect()->route('config.update');
    }

    public function restart()
    {
        Artisan::call('queue:restart', []);
        session()->flash('info', __('Restarted has been triggered for configured queue workers.'));
        return redirect()->route('config.update');
    }
}
