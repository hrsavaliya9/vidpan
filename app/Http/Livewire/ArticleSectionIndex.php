<?php

namespace App\Http\Livewire;

use App\ArticleSection;
use Livewire\Component;
use Livewire\WithPagination;

class ArticleSectionIndex extends Component
{
    use WithPagination;

    public $search;

    public $length;

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = ArticleSection::query();
        if ($this->search) {
            $query->where('name', 'like', "%$this->search%");
        }

        $sections = $query->latest()->paginate($this->length);
        return view('livewire.article-section-index', compact('sections'));
    }
}
