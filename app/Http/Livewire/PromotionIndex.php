<?php

namespace App\Http\Livewire;

use App\Promotion;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class PromotionIndex extends Component
{
    use WithPagination;

    public $search;

    public $length;

    public $sticky;

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function updatingSticky()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = Promotion::query();
        if ($this->search) {
            $query->where('title', 'like', "%$this->search%");
        }

        if ($this->sticky) {
            $query->where('sticky', $this->sticky === 'true');
        }

        $promotions = $query->latest()->paginate($this->length);
        return view('livewire.promotion-index', compact('promotions'));
    }
}
