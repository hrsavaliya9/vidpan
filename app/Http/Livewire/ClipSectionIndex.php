<?php

namespace App\Http\Livewire;

use App\ClipSection;
use Livewire\Component;
use Livewire\WithPagination;

class ClipSectionIndex extends Component
{
    use WithPagination;

    public $search;

    public $length;

    public function mount()
    {
        $this->length = '10';
    }

    public function updatingLength()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $query = ClipSection::query();
        if ($this->search) {
            $query->where('name', 'like', "%$this->search%");
        }

        $sections = $query->latest()->paginate($this->length);
        return view('livewire.clip-section-index', compact('sections'));
    }
}
