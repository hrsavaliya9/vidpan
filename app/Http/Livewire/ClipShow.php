<?php

namespace App\Http\Livewire;

use App\Clip;
use Livewire\Component;

class ClipShow extends Component
{
    public $clip;

    public function mount(Clip $clip)
    {
        $this->clip = $clip;
    }

    public function render()
    {
        $activities = $this->clip->activities()->latest()->paginate();
        return view('livewire.clip-show', compact('activities'));
    }
}
