<?php

namespace App\Http\Controllers;

use App\Http\Resources\Notification as NotificationResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;

class NotificationController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('viewAny', DatabaseNotification::class);
        /** @var User $user */
        $user = $request->user();
        $results = $user->notifications()->latest()->paginate(15);
        return NotificationResource::collection($results);
    }

    public function show(DatabaseNotification $notification)
    {
        $this->authorize('view', $notification);
        $notification->update(['read_at' => now()]);
        return NotificationResource::make($notification);
    }

    public function destroy(DatabaseNotification $notification)
    {
        $this->authorize('delete', $notification);
        $notification->delete();
    }

    public function unread(Request $request)
    {
        $this->authorize('viewAny', DatabaseNotification::class);
        /** @var User $user */
        $user = $request->user();
        $count = $user->unreadNotifications()->count();
        return compact('count');
    }
}
