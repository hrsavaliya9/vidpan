<?php

namespace App\Jobs;

use App\Clip;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Driver\FFMpegDriver;
use FFMpeg\Format\Video\X264;
use FFMpeg\Media\Video;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;

class UploadClipManually implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * UploadClipManually constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $data['video'] = 'videos/' . Str::random(15) . '.mp4';
        $data['screenshot'] = 'screenshots/' . Str::random(15) . '.png';
        $data['preview'] = 'previews/' . Str::random(15) . '.gif';
        $media = FFMpeg::open($this->data['video'])
            ->export()
            ->inFormat(new X264('aac', 'libx264'))
            ->toDisk(config('filesystems.cloud'));
        if (config('filesystems.cloud') !== 'public') {
            $media->withVisibility('public');
        }

        $media->save($data['video']);
        $data['duration'] = $media->getDurationInSeconds();
        $media = FFMpeg::open($this->data['video'])
            ->frame(TimeCode::fromSeconds(1))
            ->export()
            ->toDisk(config('filesystems.cloud'));
        if (config('filesystems.cloud') !== 'public') {
            $media->withVisibility('public');
        }

        $media->save($data['screenshot']);
        $ffmpeg = FFMpeg::open($this->data['video']);
        /** @var Video $video */
        $video = $ffmpeg->getDriver()->get();
        $dimension = $video->getStreams()->first()->getDimensions();
        $driver = $ffmpeg->getFFMpegDriver();
        $temp = storage_path('app/temp/' . Str::random(15) . '.gif');
        $this->createGif($driver, $video->getPathfile(), $temp, $dimension);
        Storage::cloud()->put($data['preview'], file_get_contents($temp), 'public');
        unlink($temp);
        Storage::delete($this->data['video']);
        /** @var Clip $clip */
        $clip = Clip::make($data);
        $clip->user_id = $data['user'];
        $clip->approved = true;
        $clip->save();
        if ($data['sections'] ?? null) {
            $clip->sections()->attach($data['sections']);
        }

        if ($clip->description) {
            dispatch(new FindMentionsHashtags($clip, $clip->description));
        }
    }

    private function createGif(FFMpegDriver $driver, string $video, string $gif, Dimension $dimension)
    {
        $commands[] = '-ss';
        $commands[] = '1';
        $commands[] = '-t';
        $commands[] = '2';
        $commands[] = '-i';
        $commands[] = $video;
        $commands[] = '-vf';
        $commands[] = 'fps=5,scale=' . $dimension->getWidth() . ':-1';
        $commands[] = '-gifflags';
        $commands[] = '+transdiff';
        $commands[] = '-y';
        $commands[] = $gif;
        $driver->command($commands);
    }
}
