<?php

return [
    'clip_approved' => [
        'title' => 'Uploaded clip is now approved.',
        'body' => 'Your video clip has been approved and should be visible to everyone.',
    ],
    'commented_on_your_clip' => [
        'title' => '@:user commented on your clip.',
        'body' => 'Click here to see their comment and reply.',
    ],
    'follow_suggestion' => [
        'title' => 'Follow @:user now.',
        'body' => 'To see interesting short clips uploaded by them.',
    ],
    'liked_your_clip' => [
        'title' => '@:user liked on your clip.',
        'body' => 'Click here to see likes on your clip.',
    ],
    'mentioned_you_in_comment' => [
        'title' => '@:user commented on your clip.',
        'body' => 'Click here to see their comment and reply.',
    ],
    'posted_new_clip' => [
        'title' => '@:user posted a new video clip.',
        'body' => 'Click here to watch it and enjoy your time.',
    ],
    'sent_you_message' => [
        'title' => '@:user has sent you a message.',
    ],
    'started_following_you' => [
        'title' => '@:user has started following you.',
        'body' => 'Click here to see their profile and follow back.',
    ],
    'tagged_you_in_clip' => [
        'title' => '@:user tagged you in a clip.',
        'body' => 'Click here to see the clip and do not forget to share.',
    ],
    'verification_approved' => [
        'title' => 'Your account is now verified.',
        'body' => 'Congratulations. Your verification request has been approved.',
    ],
];
